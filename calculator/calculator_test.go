package calculator

import (
	"errors"
	"testing"
)

func TestCalculate_Positive(t *testing.T) {
	t.Parallel()

	t.Run("Test Sum", func(t *testing.T) {
		t.Parallel()

		tests := []struct {
			operand1 float64
			operator string
			operand2 float64
			expected float64
			title    string
		}{
			{1, "+", 1, 2, "Sum two integers"},
			{5.5, "+", 3, 8.5, "Sum float and integer"},
			{5.5, "+", -3, 2.5, "Sum float and negative integer"},
			{1.543, "+", 1.457, 3, "Sum floats"},
		}

		for _, test := range tests {

			result, err := Calculate(test.operand1, test.operand2, test.operator)
			if err != nil {
				t.Errorf("Test: '%s'. Error: %v", test.title, err)
			}

			if result != test.expected {
				t.Errorf("Test: '%s'. Expected result: %v, got: %v", test.title, test.expected, result)
			}

		}
	})

	t.Run("Test Substruction", func(t *testing.T) {
		t.Parallel()

		tests := []struct {
			operand1 float64
			operator string
			operand2 float64
			expected float64
			title    string
		}{
			{1, "-", 1, 0, "Substruct two integers"},
			{5.5, "-", 3, 2.5, "Substruct float and integer"},
			{1.543, "-", 1.542, 0.0009999999999998899, "Substruct floats"},
			{5, "-", 10, -5.0, "Substruct"},
			{5, "-", -10, 15.0, "Substruct negative number"},
		}

		for _, test := range tests {

			result, err := Calculate(test.operand1, test.operand2, test.operator)
			if err != nil {
				t.Errorf("Test: '%s'. Error: %v", test.title, err)
			}

			if result != test.expected {
				t.Errorf("Test: '%s'. Expected result: %v, got: %v", test.title, test.expected, result)
			}

		}
	})

	t.Run("Test Multiplication", func(t *testing.T) {
		t.Parallel()

		tests := []struct {
			operand1 float64
			operator string
			operand2 float64
			expected float64
			title    string
		}{
			{1, "*", 1, 1, "Multiply two integers"},
			{5.5, "*", 3, 16.5, "Multiply float and integer"},
			{1.543, "*", 1.457, 2.248151, "Multiply floats"},
			{5, "*", -10, -50, "Multiply negative and positive numbers"},
			{-5, "*", -10, 50, "Multiply negative numbers"},
		}

		for _, test := range tests {

			result, err := Calculate(test.operand1, test.operand2, test.operator)
			if err != nil {
				t.Errorf("Test: '%s'. Error: %v", test.title, err)
			}

			if result != test.expected {
				t.Errorf("Test: '%s'. Expected result: %v, got: %v", test.title, test.expected, result)
			}

		}
	})

	t.Run("Test Division", func(t *testing.T) {
		t.Parallel()

		tests := []struct {
			operand1 float64
			operator string
			operand2 float64
			expected float64
			title    string
		}{
			{1, "/", 1, 1, "Divide two integers"},
			{5.5, "/", 2, 2.75, "Divide float and integer"},
			{1.543, "/", 1.534, 1.0058670143415906, "Divide floats"},
			{5, "/", -10, -0.5, "Divide negative and positive numbers"},
			{-5, "/", -10, 0.5, "Divide negative numbers"},
			{0, "/", 10, 0, "Divide zero by number"},
		}

		for _, test := range tests {

			result, err := Calculate(test.operand1, test.operand2, test.operator)
			if err != nil {
				t.Errorf("Test: '%s'. Error: %v", test.title, err)
			}

			if result != test.expected {
				t.Errorf("Test: '%s'. Expected result: %v, got: %v", test.title, test.expected, result)
			}

		}
	})
}

func TestCalculate_Negative(t *testing.T) {
	t.Parallel()

	t.Run("Test Divide by Zero", func(t *testing.T) {
		t.Parallel()

		operand1 := 5.5
		operand2 := 0.0
		operator := "/"
		_, err := Calculate(operand1, operand2, operator)

		if err == nil {
			t.Errorf("Expected error: division by zero is not allowed")
		}
	})

	t.Run("Test Invalid Operator", func(t *testing.T) {
		t.Parallel()

		operand1 := 5.5
		operand2 := 0.0

		// we can use the list of operators based on ASCII table, but for simplicity we will use simple invalid operator
		tests := []struct {
			operator string
			title    string
		}{
			{"!", "Invalid operator"},
			{" +", "White space before operator"},
			{"+ ", "White space after operator"},
			{"+1", "Operator with number"},
			{"", "Missing Operator"},
			{" ", "Simple White space"},
			{"++", "Double operator"},
			{"+ -", "Two operators"},
			{"abs", "Simple text"},
		}

		expectedError := errors.New("invalid operator")

		for _, test := range tests {

			_, err := Calculate(operand1, operand2, test.operator)

			if err != nil && err.Error() != expectedError.Error() {
				t.Errorf("Test: '%s' - Expected error: %v, got: %v", test.title, expectedError, err)
			}

		}
	})
}

func TestParseOperand(t *testing.T) {
	t.Parallel()

	t.Run("Test Parse Valid Operand", func(t *testing.T) {
		t.Parallel()

		tests := []struct {
			input    string
			expected float64
			err      error
		}{
			{"1", 1, nil},
			{"3.14", 3.14, nil},
			{"-5", -5, nil},
		}

		for _, test := range tests {

			result, err := ParseOperand(test.input)

			if err != nil && err.Error() != test.err.Error() {
				t.Errorf("Expected error: %v, got: %v", test.err, err)
			}

			if result != test.expected {
				t.Errorf("Expected result: %v, got: %v", test.expected, result)
			}

		}
	})

	t.Run("Test Parse Invalid Operand", func(t *testing.T) {
		t.Parallel()

		tests := []struct {
			input string
			title string
		}{
			{"1.1.1", "Pretend to be float"},
			{"", "Missing any value"},
			{" ", "Simple White space"},
			{"1 2", "White space between int"},
			{"%", "Special char"},
			{"1+1", "Operator between int"},
			{"abc", "Simple text"},
			{" 2", "White space before int"},
			{"2 ", "White space after int"},
			{"2a", "Int with char"},
			{"2,2", "Float with comma"},
			{"2f", "I want to be float"},
		}

		expectedError := errors.New("invalid operand")

		for _, test := range tests {

			_, err := ParseOperand(test.input)

			if err != nil && err.Error() != expectedError.Error() {
				t.Errorf("Test: '%s' ('%s') - Expected error: %v, got: %v", test.title, test.input, expectedError, err)
			}

		}
	})
}
