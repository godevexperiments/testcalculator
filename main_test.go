package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
	"testing"

	"github.com/cucumber/godog"
	"github.com/stretchr/testify/assert"
)

type ctxKeyInput struct{}

type ctxKeyPath struct{ path string }

func TestFeatures(t *testing.T) {

	suite := godog.TestSuite{
		ScenarioInitializer: func(ctx *godog.ScenarioContext) {
			ctx.Step(`^I have a calculator in path: "([^"]*)"$`, iHaveCalculator)
			ctx.Step(`^I enter: "([^"]*)"$`, iEnter)
			ctx.Step(`^the result should be "([^"]*)"$`, theResultShouldBe)
			ctx.Step(`^the error message should be "([^"]*)"$`, theErrorMessageShouldBe)
		},

		Options: &godog.Options{
			Format:   "pretty",
			Paths:    []string{"features"},
			TestingT: t,
		},
	}

	if suite.Run() != 0 {
		t.Fatal("non-zero status returned, failed to run feature tests")
	}
}

// iHaveCalculator is used to be able to retrieve the path of the calculator.
func iHaveCalculator(ctx context.Context, path string) (context.Context, error) {
	var t asserter
	log.Printf("I should have file here: %s", path)
	pathValue := ctxKeyPath{path}
	_, err := os.Stat(path)
	if assert.NoError(&t, err) {
		return context.WithValue(ctx, ctxKeyPath{}, pathValue), nil
	}

	return ctx, assertExpectedAndActual(assert.Equal, nil, err.Error(), "I should have file here: %s", path)
}

// iEnter is used to be able to retrieve the user input.
func iEnter(ctx context.Context, userInputs string) context.Context {
	userInput := strings.Split(userInputs, " ")
	return context.WithValue(ctx, ctxKeyInput{}, userInput)
}

// theResultShouldBeOnTheScreen is used to be able to retrieve the result of the calculation.
func theResultShouldBe(ctx context.Context, expectedOutput string) error {
	var t asserter
	path := ctx.Value(ctxKeyPath{}).(ctxKeyPath)
	userInput := ctx.Value(ctxKeyInput{}).([]string)

	stdout, stderr, err := runMainApp(path.path, userInput)

	if assert.NoError(&t, err) {
		return assertExpectedAndActual(assert.Contains, expectedOutput, stdout, "Invalid calculation")
	}

	return assertActual(assert.Empty, stderr, "stderr should be empty for successful operation")
}

// theErrorMessageShouldBe is used to be able to retrieve the error message.
func theErrorMessageShouldBe(ctx context.Context, expectedOutput string) error {
	var t asserter
	path := ctx.Value(ctxKeyPath{}).(ctxKeyPath)
	userInput := ctx.Value(ctxKeyInput{}).([]string)
	stdout, stderr, err := runMainApp(path.path, userInput)

	if !assert.NoError(&t, err) {
		return assertExpectedAndActual(assert.Contains, stderr, expectedOutput, "Invalid error message")
	}

	return assertActual(assert.Empty, stdout, "stdout should be empty for successful operation")
}

// assertExpectedAndActual is used to be able to assert the expected and actual values.
func assertExpectedAndActual(a expectedAndActualAssertion, expected, actual interface{}, msgAndArgs ...interface{}) error {
	var t asserter
	a(&t, expected, actual, msgAndArgs...)
	return t.err
}

// expectedAndActualAssertion is used to be able to assert the expected and actual values.
type expectedAndActualAssertion func(t assert.TestingT, expected, actual interface{}, msgAndArgs ...interface{}) bool

// assertExpectedAndActual is used to be able to assert the expected and actual values.
func assertActual(a actualAssertion, actual interface{}, msgAndArgs ...interface{}) error {
	var t asserter
	a(&t, actual, msgAndArgs...)
	return t.err
}

// actualAssertion is used to be able to assert the expected and actual values.
type actualAssertion func(t assert.TestingT, actual interface{}, msgAndArgs ...interface{}) bool

// asserter is used to be able to capture the error.
type asserter struct {
	err error
}

// Errorf is used to be able to capture the error.
func (a *asserter) Errorf(format string, args ...interface{}) {
	a.err = fmt.Errorf(format, args...)
}

// runMainApp runs the compiled application with the given arguments and captures stdout and stderr.
func runMainApp(path string, args []string) (stdout, stderr string, err error) {
	cmd := exec.Command(path, args...)
	var outb, errb strings.Builder
	cmd.Stdout = &outb
	cmd.Stderr = &errb

	err = cmd.Run()
	stdout = strings.TrimSuffix(outb.String(), "\n")
	stderr = strings.TrimSuffix(errb.String(), "\n")

	return
}
