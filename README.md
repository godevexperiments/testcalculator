# Calculator in Go

| Branch |                                                                                                     Pipeline                                                                                                        |                                                                                                                                                                                                          Code coverage |
|--------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| master | [![pipeline status](https://gitlab.com/godevexperiments/testcalculator/badges/main/pipeline.svg)](https://gitlab.com/godevexperiments/testcalculator/-/commits/main) | [![coverage report](https://gitlab.com/godevexperiments/testcalculator/badges/main/coverage.svg)](https://testcalculator-godevexperiments-c5fa4c0d9ab3f35518c1293823d685d.gitlab.io/coverage-report.html) |

## Hello.

I've prepared my solution. It took a bit over 2.5 hours (around 3), but overall, I managed to stay within the allotted time. I wouldn't say I've developed a full-fledged framework. Speaking of a "fully functional" framework, I would prefer to build it based on allure-go. But since I only have 2 hours, I will try to use the simplest and fastest solutions. To be honest, I couldn't create a single framework for both integration and unit tests. Specifically because I decided to use godog lib, and making BDD for unit tests isn't sensible. Generally, I'm not a big fan of this approach, but the godog library itself is very simple and allows for writing tests that are understandable for both developers and managers. Moreover, it's easily extendable with additional modules. Unit tests are usually intended for developers, so visibility and readability isn't a crucial factor. Thus, we'll keep unit tests in their usual form. I've slightly expanded the set of unit tests. For further development, I would improve the table-based tests - turn them into subtests. I decided to run integration tests on the compiled application. This seemed reasonable since the presented calculator is comparatively a simple application essentially consisting of a single module. My approach allows combining integration and e2e testing (this is another reason for choosing BDD). For CICD, I use Gitlab. The main reason is that I have more experience with it than with other tools. The entire pipeline consists of several stages:

*lint* - code quality check

*test* - unit tests

*build* - just build

*int_test* - integration tests

*deploy* - report publish
    
Gitlab Artifacts are used for storing results. Gitlab generates two types of reports - code coverage and reports for integration tests. The results of unit tests are not generated separately and are available only when viewing logs.
The code coverage report is accessible through a link in the Readme (click on the code coverage badge). The report for integration tests is available in the Tests tab in Pipelines. I agree that this solution doesn't meet the requirements 100%, but it fits within the stated 2.5 hours. I'm ready to answer questions and provide further explanations and am open to any feedback.