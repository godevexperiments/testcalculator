Feature:
    For integration tests we veify how all components work together.
    We already have unit tests so we can skip math tests.

    Background:
        Given I have a calculator in path: ./bin/calculator
        # "C:\\Users\\maxte\\go\\bin\\CALCULATOR_ESUSU.exe"

    Scenario Outline: Calculation Test - Perform basic math operations and veirfy output
        When I enter: "<userInput>"
        Then the result should be "<result>"

        Examples:
            | userInput | result             |
            | 10 + 5    | 10 + 5 = 15.000000 |
            | 10 - 5    | 10 - 5 = 5.000000  |
            | 10 * 5    | 10 * 5 = 50.000000 |
            | 10 / 5    | 10 / 5 = 2.000000  |
    
    Scenario: Calculation Test - Perform division by zero and veirfy error message
        When I enter: "10 / 0"
        Then the error message should be "division by zero is not allowed"

    Scenario Outline: Argument Validation Tests - Verify "Usage: main <operand1> <operator> <operand2>" error message is returned
        When I enter: "<userInput>"
        Then the error message should be "<result>"

        Examples:
            | userInput | result                                       |
            | 10 +      | Usage: main <operand1> <operator> <operand2> |
            | -         | Usage: main <operand1> <operator> <operand2> |
            | 10 5      | Usage: main <operand1> <operator> <operand2> |
            | + 10      | Usage: main <operand1> <operator> <operand2> |
            | 2 + 1 - 2 | Usage: main <operand1> <operator> <operand2> |
            |           | Usage: main <operand1> <operator> <operand2> |
            | 1+1       | Usage: main <operand1> <operator> <operand2> |

    Scenario Outline: Operand Parsing Tests - Verify "invalid operand" error message is returned when entering invalid operand
        When I enter: "<userInput>"
        Then the error message should be "invalid operand"

        Examples:
            | userInput |
            | a + 5     | 
            | 1 + b     |
            | 1 + +     |
            | 1 ^ b     |

    Scenario Outline: Operand Parsing Tests - Verify "invalid operand" error message is returned when entering invalid operand
        When I enter: "5 x 5"
        Then the error message should be "invalid operator"

        Examples:
            | userInput |
            | 5 x 5     | 
            | 1 -- b    |
            | 1 ** b    |
            